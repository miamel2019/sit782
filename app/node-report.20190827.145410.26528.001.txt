================================================================================
==== Node Report ===============================================================

Event: exception, location: "OnUncaughtException"
Filename: node-report.20190827.145410.26528.001.txt
Dump event time:  2019/08/27 14:54:10
Module load time: 2019/08/27 14:54:09
Process ID: 26528
Command line: node index.js 

Node.js version: v8.16.0
(http_parser: 2.8.0, v8: 6.2.414.77, uv: 1.23.2, zlib: 1.2.11, ares: 1.10.1-DEV,
 modules: 57, nghttp2: 1.33.0, napi: 4, openssl: 1.0.2r, icu: 60.1, unicode: 10.0,
 cldr: 32.0, tz: 2017c)

node-report version: 2.2.1 (built against Node.js v8.16.0, glibc 2.27, 64 bit)

OS version: Linux 4.15.0-54-generic #58-Ubuntu SMP Mon Jun 24 10:55:24 UTC 2019
(glibc: 2.27)

Machine: sajesh x86_64

================================================================================
==== JavaScript Stack Trace ====================================================

Object.<anonymous> (/home/sajesh/hyperledger/codes/unicoin/app/lib/User.js:10:20)
Module._compile (module.js:653:30)
Object.Module._extensions..js (module.js:664:10)
Module.load (module.js:566:32)
tryModuleLoad (module.js:506:12)
Function.Module._load (module.js:498:3)
Module.require (module.js:597:17)
require (internal/module.js:11:18)
Object.<anonymous> (/home/sajesh/hyperledger/codes/unicoin/app/index.js:3:14)
Module._compile (module.js:653:30)
Object.Module._extensions..js (module.js:664:10)
Module.load (module.js:566:32)
tryModuleLoad (module.js:506:12)
Function.Module._load (module.js:498:3)
Function.Module.runMain (module.js:694:10)
startup (bootstrap_node.js:204:16)
bootstrap_node.js:625:3

================================================================================
==== Native Stack Trace ========================================================

 0: [pc=0x7f52d06d708b] nodereport::OnUncaughtException(v8::Isolate*) [/home/sajesh/hyperledger/codes/unicoin/app/node_modules/node-report/api.node]
 1: [pc=0xeb13e2] v8::internal::Isolate::Throw(v8::internal::Object*, v8::internal::MessageLocation*) [node]
 2: [pc=0x101166b]  [node]
 3: [pc=0x1012a11] v8::internal::Runtime_DefineClass(int, v8::internal::Object**, v8::internal::Isolate*) [node]
 4: [pc=0x1bff30523918] 

================================================================================
==== JavaScript Heap and Garbage Collector =====================================

Heap space name: new_space
    Memory size: 16,777,216 bytes, committed memory: 16,732,488 bytes
    Capacity: 8,249,344 bytes, used: 5,232,496 bytes, available: 3,016,848 bytes
Heap space name: old_space
    Memory size: 17,100,800 bytes, committed memory: 15,707,840 bytes
    Capacity: 15,870,856 bytes, used: 15,419,936 bytes, available: 450,920 bytes
Heap space name: code_space
    Memory size: 2,097,152 bytes, committed memory: 1,368,704 bytes
    Capacity: 1,303,168 bytes, used: 1,303,168 bytes, available: 0 bytes
Heap space name: map_space
    Memory size: 1,593,344 bytes, committed memory: 1,196,592 bytes
    Capacity: 1,546,400 bytes, used: 1,161,776 bytes, available: 384,624 bytes
Heap space name: large_object_space
    Memory size: 2,928,640 bytes, committed memory: 2,928,640 bytes
    Capacity: 1,461,830,816 bytes, used: 2,872,992 bytes, available: 1,458,957,824 bytes

Total heap memory size: 40,497,152 bytes
Total heap committed memory: 37,934,264 bytes
Total used heap memory: 25,990,368 bytes
Total available heap memory: 1,462,810,216 bytes

Heap memory limit: 1,501,560,832

================================================================================
==== Resource Usage ============================================================

Process total resource usage:
  User mode CPU: 0.302643 secs
  Kernel mode CPU: 0.572185 secs
  Average CPU Consumption : 87.4828%
  Maximum resident set size: 68,444,160 bytes
  Page faults: 1 (I/O required) 15263 (no I/O required)
  Filesystem activity: 256 reads 8 writes

Event loop thread resource usage:
  User mode CPU: 0.259740 secs
  Kernel mode CPU: 0.566705 secs
  Average CPU Consumption : 82.6445%
  Filesystem activity: 256 reads 8 writes

================================================================================
==== Node.js libuv Handle Summary ==============================================

(Flags: R=Ref, A=Active)
Flags  Type      Address             Details
[-A]   async     0x0000000003bb3da0  
[--]   check     0x00007fff467af350  
[R-]   idle      0x00007fff467af3c8  
[--]   prepare   0x00007fff467af440  
[--]   check     0x00007fff467af4b8  
[-A]   async     0x0000000002186780  
[-A]   async     0x0000000003bbc090  
[-A]   async     0x00007f52cc0021f0  
[R-]   timer     0x0000000003bf1a40  repeat: 0, timeout expired: 15075912 ms ago
[R-]   tty       0x0000000003bbc678  width: 168, height: 38, file descriptor: 9, write queue size: 0, readable, writable
[-A]   signal    0x0000000003bf1b60  signum: 28 (SIGWINCH)
[R-]   tty       0x0000000003bf1cb8  width: 168, height: 38, file descriptor: 11, write queue size: 0, readable, writable
[-A]   async     0x00007f52cc002940  
[-A]   async     0x00007f52c40018a0  
[-A]   async     0x0000000003bda7c0  
[-A]   async     0x00007f52c4001780  
[-A]   async     0x00007f52c8000d90  
[-A]   async     0x00007f52bc002570  
[-A]   async     0x00007f52cc005af0  
[-A]   async     0x00007f52bc002600  
[-A]   async     0x00007f52bc005da0  
[-A]   async     0x00007f52d08dfce0  

================================================================================
==== System Information ========================================================

Environment variables
  NVM_DIR=/home/sajesh/.nvm
  LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:
  LC_MEASUREMENT=en_US.UTF-8
  SSH_CONNECTION=192.168.0.106 51132 192.168.0.107 22
  LESSCLOSE=/usr/bin/lesspipe %s %s
  LC_PAPER=en_US.UTF-8
  LC_MONETARY=en_US.UTF-8
  LANG=en_US.UTF-8
  OLDPWD=/home/sajesh/hyperledger/codes
  NVM_CD_FLAGS=
  LC_NAME=en_US.UTF-8
  XDG_SESSION_ID=4
  USER=sajesh
  PWD=/home/sajesh/hyperledger/codes/unicoin/app
  HOME=/home/sajesh
  SSH_CLIENT=192.168.0.106 51132 22
  XDG_DATA_DIRS=/usr/local/share:/usr/share:/var/lib/snapd/desktop
  LC_ADDRESS=en_US.UTF-8
  LC_NUMERIC=en_US.UTF-8
  SSH_TTY=/dev/pts/2
  MAIL=/var/mail/sajesh
  TERM=xterm-256color
  SHELL=/bin/bash
  NVM_BIN=/home/sajesh/.nvm/versions/node/v8.16.0/bin
  SHLVL=1
  LC_TELEPHONE=en_US.UTF-8
  LOGNAME=sajesh
  XDG_RUNTIME_DIR=/run/user/1000
  PATH=/home/sajesh/.nvm/versions/node/v8.16.0/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
  LC_IDENTIFICATION=en_US.UTF-8
  LESSOPEN=| /usr/bin/lesspipe %s
  LC_TIME=en_US.UTF-8
  _=/home/sajesh/.nvm/versions/node/v8.16.0/bin/node
  SUPPRESS_NO_CONFIG_WARNING=y

Resource limits                        soft limit      hard limit
  core file size (blocks)                       0       unlimited
  data seg size (kbytes)                unlimited       unlimited
  file size (blocks)                    unlimited       unlimited
  max locked memory (bytes)              16777216        16777216
  max memory size (kbytes)              unlimited       unlimited
  open files                              1048576         1048576
  stack size (bytes)                      8388608       unlimited
  cpu time (seconds)                    unlimited       unlimited
  max user processes                         3698            3698
  virtual memory (kbytes)               unlimited       unlimited

Loaded libraries
  linux-vdso.so.1
  /lib/x86_64-linux-gnu/libdl.so.2
  /lib/x86_64-linux-gnu/librt.so.1
  /usr/lib/x86_64-linux-gnu/libstdc++.so.6
  /lib/x86_64-linux-gnu/libm.so.6
  /lib/x86_64-linux-gnu/libgcc_s.so.1
  /lib/x86_64-linux-gnu/libpthread.so.0
  /lib/x86_64-linux-gnu/libc.so.6
  /lib64/ld-linux-x86-64.so.2
  /home/sajesh/hyperledger/codes/unicoin/app/node_modules/node-report/api.node

================================================================================
