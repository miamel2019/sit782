'use strict';

const config = require('../helper/config');
const NetworkConnection = require('composer-client').BusinessNetworkConnection;
const UserAbstract = require('./UserAbstract');

/**
 * Main class for unicoin application
 */
class User extends UserAbstract{
    /**
     * @description Initialize BusinessNetworkConnection object
     */
    constructor() {
        super();
        this.bizNetworkConnection = new NetworkConnection();
    }

    /**
     * @description Add new user
     */
    async addUser (userId, username, admin, banker) {
        this.bizNetworkDefinition = await this.bizNetworkConnection.connect(config.cardName);
        let factory = await this.bizNetworkDefinition.getFactory();
        let participantRegistry = await this.bizNetworkConnection.getParticipantRegistry('deakin.edu.au.User');

        let user = await factory.newResource('deakin.edu.au', 'User', userId);

        user.admin = await factory.newRelationship('deakin.edu.au', 'Administrator', admin);
        user.banker = await factory.newRelationship('deakin.edu.au', 'Banker', banker);
        user.name = username;

        await participantRegistry.add(user);

        await this.generateUserWallet(user);
        await this.generateUserCard('deakin.edu.au.User#' + user.userId, user.userId);

        this.exportUserCard(user.userId);

        await this.bizNetworkConnection.disconnect();
    }
}

module.exports = User;
