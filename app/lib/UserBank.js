'use strict';

const config = require('../helper/config');
const NetworkConnection = require('composer-client').BusinessNetworkConnection;
const UserAbstract = require('./UserAbstract');

class UserBank extends UserAbstract {

    constructor() {
        super();
        this.bizNetworkConnection = new NetworkConnection();
    }

    async addAdmin(userId, username, phone, email) {
        this.bizNetworkDefinition = await this.bizNetworkConnection.connect(config.cardName);
        let factory = await this.bizNetworkDefinition.getFactory();
        let participantRegistry = await this.bizNetworkConnection.getParticipantRegistry('deakin.edu.au.Banker');

        let user = await factory.newResource('deakin.edu.au', 'Banker', userId);

        user.name = username;
        user.phone = phone;
        user.email = email;

        await participantRegistry.add(user);

        await this.generateUserCard('deakin.edu.au.Banker#' + user.bankerId, user.bankerId);

        this.exportUserCard(user.bankerId);

        await this.bizNetworkConnection.disconnect();
    }

}

module.exports = UserBank;
