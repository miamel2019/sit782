'use strict';

const config = require('../helper/config');
const NetworkConnection = require('composer-client').BusinessNetworkConnection;
const UserAbstract = require('./UserAbstract');

class UserAdmin extends UserAbstract {

    constructor() {
        super();
        this.bizNetworkConnection = new NetworkConnection();
    }

    async addAdmin(userId, username) {
        this.bizNetworkDefinition = await this.bizNetworkConnection.connect(config.cardName);
        let factory = await this.bizNetworkDefinition.getFactory();
        let participantRegistry = await this.bizNetworkConnection.getParticipantRegistry('deakin.edu.au.Administrator');

        let user = await factory.newResource('deakin.edu.au', 'Administrator', userId);

        user.name = username;

        await participantRegistry.add(user);

        await this.generateUserCard('deakin.edu.au.Administrator#' + user.adminId, user.adminId);

        this.exportUserCard(user.adminId);

        await this.bizNetworkConnection.disconnect();
    }

}

module.exports = UserAdmin;
