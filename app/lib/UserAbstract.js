'user strict';

const config = require('../helper/config');
const AdminConnection = require('composer-admin').AdminConnection;
const IdCard = require('composer-common').IdCard;
const connectionProfile = require('/home/sajesh/hyperledger/codes/unicoin/app/connection-profile.json');
const CardExport = require('composer-cli').Card.Export;

class UserAbstract {
    
    async generateUserCard (card, userId) {
        let adminConnection = new AdminConnection();

        try {
            let result = await this.bizNetworkConnection.issueIdentity(card, userId);
            //console.log(result);

            const metadata = {
                userName: result.userID,
                version: config.hlfVersion,
                enrollmentSecret: result.userSecret, //use dynamic secret
                businessNetwork: config.network
            };
            const idCardData = new IdCard(metadata, connectionProfile);

            const imported = await adminConnection.importCard(result.userID+'@'+config.network, idCardData);

            return imported;
        } catch (err) {
            throw err;
        }
    }
    
	
    exportUserCard(userId) {
        let cardName = userId+'@'+config.network;
        let options = {
          file: config.cardsDir + cardName,
          card: cardName
        };

        CardExport.handler(options);
    }

    
	
    async generateUserWallet (user) {
        let factory = await this.bizNetworkDefinition.getFactory();
        let walletRegistry = await this.bizNetworkConnection.getAssetRegistry('deakin.edu.au.Wallet');

        let wallet = await factory.newResource('deakin.edu.au', 'Wallet', 'wallet'+user.userId);

        wallet.symbol = 'AUD';
        wallet.decimals = 18;
        wallet.token = 0;
        wallet.owner = await factory.newRelationship('deakin.edu.au', 'User', user.userId);

        return walletRegistry.add(wallet);
    }
}

module.exports = UserAbstract;
