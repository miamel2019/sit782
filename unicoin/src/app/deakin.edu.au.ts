import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace deakin.edu.au{
   export class Banker extends Participant {
      bankerId: string;
      name: string;
      phone: string;
      email: string;
   }
   export class Administrator extends Participant {
      adminId: string;
      name: string;
   }
   export class User extends Participant {
      userId: string;
      name: string;
      banker: Banker;
      admin: Administrator;
   }
   export class Token extends Asset {
      tokenId: string;
      name: string;
      symbol: string;
      decimals: number;
      amount: number;
      owner: User;
   }
   export class TokenTransfer extends Transaction {
      owner: User;
      payee: User;
      transferAmount: number;
   }
// }
