'use strict';

// HOW TO INVOLVE BANKER IN DEPOSIT/WITHDRAW TRANSACTION ???
// mia --------------------------------------------------

// check if token amount is positive -----------------------
function validateAmount(transactionAmount) {
    if (transactionAmount < 0) {
      throw new Error('Can not make invalid transaction !!')
    }
}

// token deposit into wallet  -----------------------
function tokenDeposit(depositTransaction) {
    validateAmount(depositTransaction.depositAmount)
  
    var wallet = depositTransaction.wallet
    wallet.balance += depositTransaction.depositAmount
  
// https://hyperledger.github.io/composer/v0.16/api/runtime-factory#newconcept   
// newConcept() - Create a new relationship to the transaction
    var newTransaction = getFactory().newConcept('unicoin.network', 'Transaction')
    newTransaction.transactionAmount = depositTransaction.depositAmount
    newTransaction.category = "DEPOSIT"
  
// push() - Add the newly created transaction record to the transaction array of records.
    if (wallet.transactions) {
      wallet.transactions.push(newTransaction)
    } else {
      wallet.transactions = [newTransaction]
    }
  
    return getAssetRegistry('unicoin.network.Wallet')
      .then(function (assetRegistry) {
        return assetRegistry.update(wallet)
      })
  }

// token withdrawn from wallet  -----------------------
  function tokenWithdraw(withdrawTransaction) {
    validateAmount(withdrawTransaction.withdrawAmount)
  
    var wallet = withdrawTransaction.wallet
  
    if (wallet.balance < withdrawTransaction.withdrawAmount) {
        throw new Error('Inadequate balance !!')
    } else{
        wallet.balance -= withdrawTransaction.withdrawAmount
    }
    
    var newTransaction = getFactory().newConcept('unicoin.network', 'Transaction')
    newTransaction.transactionAmount = withdrawTransaction.withdrawAmount
    newTransaction.category = "WITHDRAW"
  
    if (wallet.transactions) {
      wallet.transactions.push(newTransaction)
    } else {
      wallet.transactions = [newTransaction]
    }
  
    return getAssetRegistry('unicoin.network.Wallet')
      .then(function (assetRegistry) {
        return assetRegistry.update(wallet)
      })
  }
  
  // token transfer among users  -----------------------
  function tokenTransfer(transferTransaction) {
    validateAmount(transferTransaction.transferAmount)
  
    if (transferTransaction.transferer.balance < transferTransaction.transferAmount) {
      throw new Error('Inadequate balance !!')
    } else {
        transferTransaction.transferer.balance -= transferTransaction.transferAmount
        transferTransaction.transferee.balance += transferTransaction.transferAmount      
    }
      
    var sendTransaction = getFactory().newConcept('unicoin.network', 'Transaction')
    sendTransaction.transactionAmount = transferTransaction.transferAmount
    sendTransaction.category = "SEND"
    if (transferTransaction.transferer.transactions) {
      transferTransaction.transferer.transactions.push(sendTransaction)
    } else {
      transferTransaction.transferer.transactions = [sendTransaction]
    }

    var receiveTransaction = getFactory().newConcept('unicoin.network', 'Transaction')
    receiveTransaction.transactionAmount = transferTransaction.transferAmount
    receiveTransaction.category = "RECEIVE"
    if (transferTransaction.transferee.transactions) {
      transferTransaction.transferee.transactions.push(receiveTransaction)
    } else {
      transferTransaction.transferee.transactions = [receiveTransaction]
    }
    
    return getAssetRegistry('unicoin.network.Wallet')
      .then(function (assetRegistry) {
        return assetRegistry.updateAll([transferTransaction.transferer, transferTransaction.transferee])
      })
  }